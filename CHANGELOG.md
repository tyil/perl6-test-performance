# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic
Versioning](http://semver.org/spec/v2.0.0.html).

## [0.2.1] - 2018-07-16
### Added
- A README in Perl 6 Pod format, is now available in the repository root.

## [0.2.0] - 2018-07-09
### Changed
- Additional arguments to `pp-perform` are now named instead of positional.

## [0.1.0] - 2018-07-09
- Initial release
