#! /usr/bin/env false

use v6.c;

unit module Test::Performance;

#| Perform a block of code a number of times.
sub perform (
	&runnable,
	Int:D :$iterations where 0 < * = 50,
	Real:D :$scale where * < 1 = 0.00001,
	--> Hash
) is export {
	my @timings;
	my Duration $fastest;
	my Duration $slowest;

	for ^$iterations {
		my Instant $start = now;

		&runnable();

		@timings[$_] = now - $start;

		$fastest = @timings[$_] if !$fastest.DEFINITE || @timings[$_] < $fastest;
		$slowest = @timings[$_] if !$slowest.DEFINITE || $slowest < @timings[$_];
	}

	my $total-time = [+] @timings;
	my $average = ($total-time / $iterations).round($scale);

	%(
		total-time => $total-time.Num.round($scale),
		fastest => $fastest.Num.round($scale),
		slowest => $slowest.Num.round($scale),
		:$average,
		:$iterations,
	);
}

#| Pretty-print variant on C<perform>. Instead of a C<Hash>, it will return a
#| Str which will have pretty printed output.
sub pp-perform (
	&runnable,
	Int:D :$iterations where 0 < * = 50,
	Real:D :$scale where * < 1 = 0.00001,
	--> Str
) is export {
	my %duration = perform(&runnable, :$iterations, :$scale);
	my Str $report = "Performance report:\n\n";
	my Int $indent = 3;

	$report ~= "Execution time: %duration<total-time> (%duration<iterations>)\n".indent($indent);
	$report ~= "Average run:    %duration<average>\n".indent($indent);
	$report ~= "Best run:       %duration<fastest>\n".indent($indent);
	$report ~= "Worst run:      %duration<slowest>\n".indent($indent);
}

=begin pod

=NAME    Test::Performance
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.2.1

=head1 SYNOPSIS

=item1 C<perform(&, Int:D :$iterations, Int:D $scale --> Hash)>
=item1 C<pp-perform(&, Int:D :$iterations, Int:D :$scale --> Str)>

=head1 DESCRIPTION

Runs a block of code a number of times to gauge performance.

=head1 EXAMPLES

=head2 Intended pretty-print usage

=begin input
use My::Module;

say pp-perform({ my-exported-sub("foo", "bar") });
=end input

This will run the code block given to C<pp-perform> 50 times, and C<say> will
prin the report to C<STDOUT> once it is done. The report will contain the total
execution time of the 50 iterations, the average speed of an iteration, the best
and the worst runs.

=head2 Lower-level perform usage

=begin input
use My::Module;

my %performance = perform({ my-exported-sub("foo", "bar") });
=end input

Usage and arguments is similar to C<pp-performance>, but a Hash containing the
timings will now be returned, instead of a Str containing a default report.

=end pod

# vim: ft=perl6 noet
